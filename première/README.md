# lyceecours
Ces cours sont mes cours, publiés librement et gratuitement.
Certains cours, particulièrement ceux du début de l'année, et ceux auquels je ne peux pas assister en ordinateur, sont absents.
Pour chaque cours, deux copies sont présentes: la copie de travail avec extension .lyx et le document pleinement formatté avec extension .pdf. 